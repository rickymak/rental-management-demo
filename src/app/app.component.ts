import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DEMOANGULAR';
  myData:any;
  myData1:any;
  constructor(private http: HttpClient,private router: Router) { }
  ngOnInit() {
    this.http.get('../assets/catalog.json')
      .subscribe(res =>
         {
           this.myData= res['data'].locations;

         }         
         );

  }
  gotoDetails(data){ 
    localStorage.clear();
    localStorage.setItem('catego',JSON.stringify(data.categories) ); 
    console.log("show data soni", data.categories)
    this.router.navigate(['/screen']);
    
    // this.router.navigate(['/screen'], 
    // { queryParams: JSON.parse(data.categories)
       
    // });
    // this.router.navigate(['/screen'], 
    // { queryParams: 
    //     { sendData: JSON.stringify(data.categories) } 
    // });
  }
}
