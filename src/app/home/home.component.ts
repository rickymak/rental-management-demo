import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myData:any;
  myData1:any;
  constructor(private http: HttpClient,private router: Router) { }

  ngOnInit() {
    this.http.get('../assets/catalog.json')
      .subscribe(res =>
         {
           this.myData= res['data'].locations;
           this.myData1 = res['data']
 // console.log(res['data'].locations)
         }  
         
         );

  }
  gotoDetails(data){ 
    this.router.navigate(['/screen']) 
    // this.router.navigate(['/screen'], 
    // { queryParams: JSON.parse(data.categories)
       
    // });
    // this.router.navigate(['/screen'], 
    // { queryParams: 
    //     { sendData: JSON.stringify(data.categories) } 
    // });
  }
}
