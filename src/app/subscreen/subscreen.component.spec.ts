import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscreenComponent } from './subscreen.component';

describe('SubscreenComponent', () => {
  let component: SubscreenComponent;
  let fixture: ComponentFixture<SubscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
