import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ScreenComponent} from './screen/screen.component';
import {SubscreenComponent} from './subscreen/subscreen.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'screen', component: ScreenComponent },
  { path: 'subscreen', component: SubscreenComponent },
    { path: '', redirectTo:'/home' ,pathMatch: 'full' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
