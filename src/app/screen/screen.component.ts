import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
export class ScreenComponent implements OnInit {
  subDetails:any;
  xx : any;
  myData:any;
  constructor(private route: ActivatedRoute, private http: HttpClient,private router: Router) { }

  ngOnInit() {

    this.http.get('../assets/catalog.json')
    .subscribe(res =>
       {
         this.myData= res['data'].locations;

       }         
       );

   this.xx =JSON.parse(localStorage.getItem('catego')); 
   console.log('Screen page calll',this.xx)

  }
  gotosubcategory(dataa)
  {
    localStorage.setItem('subcatego',JSON.stringify(dataa['subcategories'])); 
    //console.log("show Sub Catergory data  ", dataa['subcategories'])
    this.router.navigate(['/subscreen']);
  }
  gotoDetails(data){ 
    this.xx =[];    
   this.xx = data.categories
  }

}
